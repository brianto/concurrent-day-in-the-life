import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Employee
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public abstract class Employee<Role> extends Thread {
	public static final Clock EARLIEST_START = new Clock(8, 0);
	public static final Clock MORNING_SETTLE_DOWN = new Clock(9, 15);
	public static final Clock CLEANUP_TIME = new Clock(16, 0);
	public static final Clock EVENING_STANDUP_LATEST = new Clock(16, 15);
	public static final Clock END_TIME = new Clock(17, 0);

	private final String name;
	private final Clock clock;
	private final PriorityQueue<Task<Role>> tasks;
	private final PriorityQueue<Task<Role>> questions;
	private final Queue<Employee<?>> inquirers;
	private final Statistics statistics;

	private volatile Task<Role> currentTask;
	private volatile boolean pendingAnswer;

	private static final Random Random = new Random();

	private boolean dayOver;

	/**
	 * Set the random seed for the Random Generator to the given number.
	 * 
	 * @param seed
	 *            The new seed for the Random number generator
	 */
	public static void useRandomSeed(int seed) {
		Employee.Random.setSeed(seed);
	}

	/**
	 * Constructor.
	 * 
	 * @param clock
	 *            Clock employee will follow throughout the day.
	 * @param name
	 *            Name of the employee.
	 */
	public Employee(Clock clock, String name) {
		this.name = name;
		this.clock = clock;

		this.pendingAnswer = false;

		this.tasks = new PriorityQueue<Task<Role>>();
		this.questions = new PriorityQueue<Task<Role>>();
		this.inquirers = new ConcurrentLinkedQueue<Employee<?>>();
		this.statistics = new Statistics();

		this.setName(this.name);
	}

	/**
	 * Schedule a task.
	 * 
	 * Note: A question may also be put into the task queue (but don't).
	 * Schedule questions with {@link Employee#scheduleQuestion(Task)}.
	 * 
	 * @param task
	 *            Task to schedule
	 */
	public void scheduleTask(Task<Role> task) {
		this.tasks.add(task);
	}

	/**
	 * Schedule a question.
	 * 
	 * Note: A task may also be put into the question queue (but don't).
	 * Schedule tasks with {@link Employee#scheduleTask(Task)}.
	 * 
	 * @param task
	 *            Question to schedule.
	 */
	public void scheduleQuestion(Task<Role> task) {
		this.questions.add(task);
	}

	/**
	 * Checks if this Employee is waiting for an answer.
	 * 
	 * @return True if waiting for an answer, false otherwise.
	 */
	public boolean pendingAnswer() {
		return this.pendingAnswer;
	}

	/**
	 * Gets a random time between the specified range.
	 * 
	 * @param start
	 *            Clock starting time range
	 * @param end
	 *            Clock end time range
	 * @param duration
	 *            Amount of time a future task will take place, this is used to
	 *            select a random value that will allow for that task to execute
	 *            within the start and end times specified.
	 */
	public Clock getRandomTimeBetween(Clock start, Clock end, int duration) {
		if (start.compareTo(end) > 0)
			return this.getRandomTimeBetween(end, start, duration);

		int minutesBetweenBounds = end.value() - start.value();

		if (minutesBetweenBounds < duration)
			return new Clock(start);

		Clock random = new Clock(start).increment(this.getRandom().nextInt(
				minutesBetweenBounds - duration));

		return random;
	}

	/**
	 * Schedule lunchtime randomly.
	 * 
	 * @param arrivalMinute
	 *            Time in minutes employee arrived to work.
	 */
	public void scheduleRandomLunch(int arrivalMinute) {
		final int maxLunchTimeDuration = 60 - arrivalMinute;
		final int lunchTimeDuration = this.getRandom().nextInt(
				maxLunchTimeDuration - 30 + 1) + 30;

		Clock lunch = this.getRandomTimeBetween(Employee.MORNING_SETTLE_DOWN,
				Employee.CLEANUP_TIME, lunchTimeDuration);

		this.scheduleTask(new Task<Role>("lunch", lunch) {
			@Override
			void action(Role self) throws Exception {
				stopRecording("working");

				log("starting lunch, should end in %d minutes",
						lunchTimeDuration);

				startRecording("lunch");

				waitFor(lunchTimeDuration);

				log("finished lunch");

				stopRecording("lunch");

				startRecording("working");
			}
		});
	}

	/**
	 * Schedule a random question.
	 */
	public void scheduleRandomQuestion() {
		Clock randomTime = this.getRandomTimeBetween(
				Employee.MORNING_SETTLE_DOWN, Employee.CLEANUP_TIME, 10);

		this.scheduleQuestion(new Task<Role>("asking question", randomTime) {
			@SuppressWarnings("unchecked")
			@Override
			void action(Role self) throws Exception {
				Employee<?> superior = ((Employee<Role>) self).getSuperior();

				stopRecording("working");

				log("asking %s a question", superior);

				startRecording("question");

				superior.ask((Employee<?>) self);

				if (!((Employee<Role>) self).pendingAnswer()) {
					log("my question answered");
				} else {
					log("could not be answered in time");
				}

				stopRecording("question");

				startRecording("working");
			}
		});
	}

	/**
	 * Checks if there is another task to do.
	 * 
	 * @return True if there is another task to do, false otherwise.
	 */
	public boolean nextTaskDue(Queue<Task<Role>> queue) {
		if (queue.isEmpty())
			return false;

		return queue.peek().shouldRun(this.clock);
	}

	/**
	 * Do the next task that needs to be done.
	 * 
	 * @param queue
	 *            Queue of tasks to do.
	 */
	public void doNextTask(Queue<Task<Role>> queue) {
		if (queue.isEmpty())
			return;

		try {
			Task<Role> task = queue.poll();

			this.currentTask = task;
			this.setName(String.format("%s %s", this.toString(),
					this.currentTask.getDescription()));

			task.doActionWrapper(this);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Dismiss all unanswered questions.
	 * 
	 * This does not unset the {@link Employee#pendingAnswer} flag like
	 * {@link Employee#addressPendingQuestion()} does. This is to be used at the
	 * end of the day to bail out all employees waiting for an answer to a
	 * question.
	 */
	public synchronized void dismissAllInquirers() {
		while (!this.inquirers.isEmpty()) {
			Employee<?> inquirer = this.inquirers.poll();

			synchronized (inquirer) {
				inquirer.notify();
			}
		}
	}

	/**
	 * Returns if there are threads waiting for an answer from this thread.
	 * 
	 * @return true if there are threads waiting for an answer from this thread
	 */
	public boolean hasQuestionPending() {
		return !this.inquirers.isEmpty();
	}

	/**
	 * Answers a pending question.
	 */
	public void addressPendingQuestion() {
		try {
			Employee<?> haplessSoul = this.inquirers.poll();

			this.answerQuestion(haplessSoul);

			synchronized (haplessSoul) {
				haplessSoul.pendingAnswer = false;
				haplessSoul.notify();
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Employee is asked asks a question.
	 * 
	 * @param haplessSoul
	 *            Employee asking the question.
	 */
	public boolean ask(Employee<?> haplessSoul) throws InterruptedException {
		haplessSoul.pendingAnswer = true;
		this.inquirers.add(haplessSoul);

		int timeout = Math.max(Employee.CLEANUP_TIME.value()
				- haplessSoul.clock.value(), 1);

		synchronized (haplessSoul) {
			// Handles "spurious wakeups"
			// http://docs.oracle.com/javase/7/docs/api/java/lang/Object.html#wait(long)
			// while (haplessSoul.pendingAnswer ||
			// Employee.CLEANUP_TIME.compareTo(haplessSoul.clock) < 0) {
			haplessSoul.wait(timeout);
			// }
		}

		boolean answered = !haplessSoul.pendingAnswer;

		haplessSoul.pendingAnswer = false;

		return answered;
	}

	/**
	 * Waits for a given number of minutes before doing anything else.
	 * 
	 * @param mins
	 *            Minutes to wait for.
	 */
	public void waitFor(int mins) {
		Clock stop = new Clock(this.clock);
		stop.increment(mins);

		while (this.clock.compareTo(stop) < 0)
			Thread.yield();
	}

	/**
	 * Checks whether the day is over or not.
	 * 
	 * @return True if the working day is over, false otherwise.
	 */
	public boolean dayOver() {
		return this.dayOver;
	}

	/**
	 * Employee leaves work.
	 */
	public void leave() {
		this.dayOver = true;
	}

	/**
	 * Track the start time of a given event
	 * 
	 * @param event
	 *            name of event to track
	 */
	public void startRecording(String event) {
		this.statistics.startRecording(event, this.clock);
	}

	/**
	 * Stops tracking the given event and adds the total time of this event to
	 * the total aggregate
	 * 
	 * @param event
	 *            name of event to track
	 */
	public void stopRecording(String event) {
		this.statistics.stopRecording(event, this.clock);
	}

	public Statistics getStatistics() {
		return this.statistics;
	}

	/**
	 * Logs a formatted message to the terminal.
	 * 
	 * @param message
	 *            Message to display.
	 */
	public void log(String message) {
		synchronized (clock) {
			System.out.println(String.format("%-10s %-15s %s",
					this.clock.toString(), this.toString(), message));
		}
	}

	/**
	 * Logs a formatted message to the terminal.
	 * 
	 * @param message
	 *            Message to display, in printf syntax.
	 * @param params
	 *            Objects that are part of the message.
	 */
	public void log(String message, Object... params) {
		log(String.format(message, params));
	}

	/**
	 * Gets the current time as a String.
	 * 
	 * @return String representation of the current time.
	 */
	public String getTime() {
		return this.clock.toString();
	}

	/**
	 * Gets the random number generator used by this Employee.
	 * 
	 * @return Random number generator used by this Employee.
	 */
	public Random getRandom() {
		// Somehow, I think Math.random() is not thread safe and duplicates
		// values.
		return Employee.Random;
	}

	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * Get the superior over this Employee
	 * 
	 * @return this employee's superior in the organizational hierarchy
	 */
	abstract Employee<?> getSuperior();

	/**
	 * Answer a question posed by the given employee
	 * 
	 * Note that this method blocks the inquirer of a response until this method
	 * terminates. Synchronization is handled in
	 * {@link Employee#addressPendingQuestion()}.
	 * 
	 * @param e
	 *            Employee that posed a question (?)
	 */
	abstract void answerQuestion(Employee<?> e) throws InterruptedException;

	@Override
	public void run() {
		try {
			this.simulate();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Run the simulation of this Employee.
	 */
	public void simulate() {
		while (!dayOver()) {
			if (Employee.CLEANUP_TIME.compareTo(this.clock) < 0)
				this.dismissAllInquirers();

			if (nextTaskDue(this.tasks)) {
				doNextTask(this.tasks);
				continue;
			}

			if (nextTaskDue(this.questions)) {
				doNextTask(this.questions);
				continue;
			}

			if (hasQuestionPending()) {
				addressPendingQuestion();
				continue;
			}

			Thread.yield();
		}
	}
}
