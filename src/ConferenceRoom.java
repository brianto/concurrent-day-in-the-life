import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CyclicBarrier;

/**
 * ConferenceRoom.java
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class ConferenceRoom {
	private CyclicBarrier barrier;
	private Queue<TeamLead> reservations;
	private volatile TeamLead currentOccupant;

	/**
	 * Constructor. Initializes the room to hold a certain amount of people.
	 * This amount of people need to be in the room before a meeting may begin.
	 * 
	 * @param size
	 *            Amount of people that need to be in the room before a meeting
	 *            may begin.
	 */
	public ConferenceRoom(int size) {
		this.barrier = new CyclicBarrier(size);
		this.reservations = new ConcurrentLinkedQueue<TeamLead>();
	}

	/**
	 * Attempts to reserve the room for use. If the current room is in use, the
	 * person trying to reserve it must wait for it to become available.
	 * 
	 * @param lead
	 *            TeamLead trying to reserve the room for use.
	 */
	public synchronized void tryUse(TeamLead lead) {
		if (this.currentOccupant == null) {
			this.currentOccupant = lead;
		} else {
			this.reservations.offer(lead);
		}
	}

	/**
	 * Checks whether the given TeamLead is using the room.
	 * 
	 * Note that the <emph>client</emph> must check whether the current
	 * conference room is "owned" by his superior. Clients are allowed to
	 * {@link CyclicBarrier#await()} even when the client's superior is not the
	 * one using this room.
	 * 
	 * @param lead
	 *            TeamLead to check against to see if he/she is in the room.
	 * @return True if used by lead, false otherwise.
	 */
	public synchronized boolean usedBy(TeamLead lead) {
		return this.currentOccupant == lead;
	}

	/**
	 * Gets the internal barrier object.
	 * 
	 * @return The internal barrier object.
	 */
	public CyclicBarrier getBarrier() {
		return this.barrier;
	}

	/**
	 * Ends the use of the room by the current occupants, allowing for others to
	 * use it.
	 */
	public void finish() {
		this.barrier.reset();

		if (this.reservations.isEmpty())
			return;

		this.currentOccupant = this.reservations.poll();
	}
}
