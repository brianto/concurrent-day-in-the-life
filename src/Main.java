import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Main
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class Main {
	/**
	 * @param args
	 *            Arguments passed in via the CLI. As it stands, args[0] should
	 *            be the random generator's seed.
	 */
	public static void main(String[] args) throws Exception {
		if (args.length == 1) {
			Employee.useRandomSeed(Integer.parseInt(args[0]));
		}

		final Clock clock = new Clock();
		ConferenceRoom managersOffice = new ConferenceRoom(4);
		ConferenceRoom smallConferenceRoom = new ConferenceRoom(4);
		ConferenceRoom largeConferenceRoom = new ConferenceRoom(12);

		Manager manager = new Manager(clock);

		Developer a = new Developer(clock, 1, 1);
		Developer b = new Developer(clock, 1, 2);
		Developer c = new Developer(clock, 1, 3);

		Developer i = new Developer(clock, 2, 1);
		Developer j = new Developer(clock, 2, 2);
		Developer k = new Developer(clock, 2, 3);

		Developer x = new Developer(clock, 3, 1);
		Developer y = new Developer(clock, 3, 2);
		Developer z = new Developer(clock, 3, 3);

		TeamLead abc = new TeamLead(clock, 1);
		TeamLead ijk = new TeamLead(clock, 2);
		TeamLead xyz = new TeamLead(clock, 3);

		manager.addLead(abc).addLead(ijk).addLead(xyz)
				.useOffice(managersOffice);

		abc.setManager(manager).addDeveloper(a).addDeveloper(b).addDeveloper(c)
				.useManagersOffice(managersOffice)
				.useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		ijk.setManager(manager).addDeveloper(i).addDeveloper(j).addDeveloper(k)
				.useManagersOffice(managersOffice)
				.useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		xyz.setManager(manager).addDeveloper(x).addDeveloper(y).addDeveloper(z)
				.useManagersOffice(managersOffice)
				.useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);

		a.setLead(abc).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		b.setLead(abc).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		c.setLead(abc).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		i.setLead(ijk).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		j.setLead(ijk).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		k.setLead(ijk).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		x.setLead(xyz).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		y.setLead(xyz).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);
		z.setLead(xyz).useSmallConferenceRoom(smallConferenceRoom)
				.useLargeConferenceRoom(largeConferenceRoom);

		final Set<Employee<?>> employees = new HashSet<Employee<?>>();
		employees.add(manager);
		employees.add(abc);
		employees.add(a);
		employees.add(b);
		employees.add(c);
		employees.add(ijk);
		employees.add(i);
		employees.add(j);
		employees.add(k);
		employees.add(xyz);
		employees.add(x);
		employees.add(y);
		employees.add(z);

		for (Employee<?> e : employees)
			e.start();

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				clock.increment();
			}
		}, 0, 10); // 10ms => 1min

		for (Employee<?> e : employees)
			e.join();

		timer.cancel();

		System.out.println("Simulation Completed");

		Statistics aggregate = new Statistics();

		for (Employee<?> e : employees)
			aggregate.add(e.getStatistics());

		System.out.printf("%-8s %8s%n", "Metric", "Minutes");
		System.out.println("=================");

		for (Map.Entry<String, Integer> entry : aggregate.asNormalizedMap()
				.entrySet()) {
			String metric = entry.getKey();
			Integer total = entry.getValue();

			System.out.printf("%8s %8d%n", metric, total);
		}
	}
}
