import java.util.HashSet;
import java.util.Set;

/**
 * Team Lead
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class TeamLead extends Employee<TeamLead> {
	private Manager manager;
	private Set<Developer> developers;
	private ConferenceRoom managersOffice;
	private ConferenceRoom smallConferenceRoom;
	private ConferenceRoom largeConferenceRoom;

	/**
	 * Constructor. Sets up the day schedule for the TeamLead.
	 * 
	 * @param clock
	 *            The clock the TeamLead will follow throughout the day.
	 * @param id
	 *            ID number of the TeamLead.
	 */
	public TeamLead(Clock clock, int id) {
		super(clock, String.format("Team Lead %d", id));

		this.developers = new HashSet<Developer>();

		// Generate a random arrival time
		Clock arrival = this.getRandomTimeBetween(Employee.EARLIEST_START,
				new Clock(8, 30), 1);

		// Calculate the finishing (work day over) time
		Clock leave = new Clock(arrival).increment(8 * 60 + 30);

		// This task encompasses the manager and team standup in one task.
		scheduleTask(new Task<TeamLead>("arriving", arrival) {
			@Override
			void action(TeamLead self) throws Exception {
				log("arrives");

				log("joining manager standup");

				startRecording("waiting");

				self.managersOffice.getBarrier().await();

				log("joined manager standup");

				stopRecording("waiting");

				log("started manager standup");

				startRecording("meeting");

				waitFor(15);

				log("leaving manager standup");

				stopRecording("meeting");

				self.smallConferenceRoom.tryUse(self);

				log("queueing for small conference room");

				startRecording("waiting");

				while (!self.smallConferenceRoom.usedBy(self))
					Thread.yield();

				log("controls small conference room");

				log("waiting for all members to join standup");

				self.smallConferenceRoom.getBarrier().await();

				stopRecording("waiting");

				log("started team standup");

				startRecording("meeting");

				waitFor(15);

				self.smallConferenceRoom.finish();

				log("finished and left standup");

				stopRecording("meeting");

				startRecording("working");
			}
		});

		// Schedule a random lunch, with maximum lunch time based on when one
		// arrives at work
		this.scheduleRandomLunch(arrival.getMinute());

		// Schedule up to 5 questions
		for (int i = 0, stop = this.getRandom().nextInt(5); i < stop; i++)
			this.scheduleRandomQuestion();

		// Generate a random arrival time at the end-of-day standup
		Clock endOfDayStandupArrival = this.getRandomTimeBetween(
				Employee.CLEANUP_TIME, Employee.EVENING_STANDUP_LATEST, 0);

		scheduleTask(new Task<TeamLead>("end-of-day standup",
				endOfDayStandupArrival) {
			@Override
			void action(TeamLead self) throws Exception {
				stopRecording("working");

				log("arrived at end-of-day standup");

				startRecording("waiting");

				self.largeConferenceRoom.getBarrier().await();

				stopRecording("waiting");

				log("started end-of-day standup");

				startRecording("meeting");

				waitFor(15);

				log("finished end-of-day standup");

				stopRecording("meeting");

				startRecording("meeting");
			}
		});

		// Leave work
		scheduleTask(new Task<TeamLead>("leaving", leave) {
			@Override
			void action(TeamLead self) throws Exception {
				stopRecording("meeting");
				
				log("leaves");

				leave();
			}
		});
	}

	/**
	 * Set the manager of the TeamLead to the given manager.
	 * 
	 * @param manager
	 *            Manager of TeamLead.
	 */
	public TeamLead setManager(Manager manager) {
		this.manager = manager;
		return this;
	}

	/**
	 * Add a developer to work under TeamLead.
	 * 
	 * @param dev
	 *            Developer to work under TeamLead.
	 */
	public TeamLead addDeveloper(Developer dev) {
		this.developers.add(dev);
		return this;
	}

	/**
	 * Team lead's specific case of answering questions.
	 * 
	 * Implicitly, all arguments are of type {@link Developer}. There is a 50%
	 * chance that this lead will know the answer. If the lead doesn't know the
	 * answer, the lead will ask his supervisor (manager). If the lead does know
	 * the answer, the response will be instantaneous.
	 * 
	 * @param e
	 *            employee who is asking a question
	 */
	@Override
	public void answerQuestion(Employee<?> e) throws InterruptedException {
		if (this.getRandom().nextBoolean()) {
			this.log("doesn't know the answer, asking %s", this.manager);

			this.manager.ask(this);
		}

		this.log("answered %s's question", e);
	}

	@Override
	public Employee<?> getSuperior() {
		return this.manager;
	}

	/**
	 * Reserve a small conference room.
	 * 
	 * @param conferenceRoom
	 *            Small ConferenceRoom to reserve.
	 * @return This TeamLead.
	 */
	public TeamLead useSmallConferenceRoom(ConferenceRoom conferenceRoom) {
		this.smallConferenceRoom = conferenceRoom;
		return this;
	}

	/**
	 * Reserve a large conference room.
	 * 
	 * @param conferenceRoom
	 *            Large ConferenceRoom to reserver
	 * @return This TeamLead.
	 */
	public TeamLead useLargeConferenceRoom(ConferenceRoom conferenceRoom) {
		this.largeConferenceRoom = conferenceRoom;
		return this;
	}

	/**
	 * ???
	 */
	public TeamLead useManagersOffice(ConferenceRoom managersOffice) {
		this.managersOffice = managersOffice;
		return this;
	}
}
