import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Statistics
 * 
 * @author Brian To
 * @author Gabriel Marcano
 * @author Alexander Kelley
 */
public class Statistics {
	private final ConcurrentHashMap<String, AtomicInteger> totals;
	private final ConcurrentHashMap<String, Clock> startTimes;

	/**
	 * Constructs an empty statistics aggregation object.
	 */
	public Statistics() {
		this.totals = new ConcurrentHashMap<String, AtomicInteger>();
		this.startTimes = new ConcurrentHashMap<String, Clock>();
	}

	/**
	 * Starts tracking total time spent on a given event.
	 * 
	 * @param event name of event to track
	 * @param clock reference to clock with the current simulation time
	 */
	public void startRecording(String event, Clock clock) {
		this.startTimes.putIfAbsent(event, new Clock(clock));
		this.totals.putIfAbsent(event, new AtomicInteger(0));
	}

	/**
	 * Updates total time counter with the total time for this event
	 * 
	 * @param event name of event whose time is to be added to the total
	 * @param clock reference to clock with the current simulation time
	 */
	public void stopRecording(String event, Clock clock) {
		Clock end = new Clock(clock);

		if (!this.startTimes.containsKey(event))
			throw new RuntimeException(String.format(
					"Key %s could not be found", event));

		Clock start = this.startTimes.get(event);

		this.totals.get(event).addAndGet(end.value() - start.value());

		this.startTimes.remove(event);
	}

	/**
	 * Merges another statistics object's sum with this object's sum
	 *  
	 * @param other stat object to merge
	 * @return this object
	 */
	public Statistics add(Statistics other) {
		for (Map.Entry<String, AtomicInteger> entry : other.totals.entrySet()) {
			String event = entry.getKey();
			AtomicInteger delta = entry.getValue();

			this.totals.putIfAbsent(event, new AtomicInteger(0));

			AtomicInteger total = this.totals.get(event);
			total.addAndGet(delta.get());
		}

		return this;
	}

	/**
	 * Returns a copy of the internal map of event to total times in a usable format.
	 * @return map of event to times (minutes)
	 */
	public Map<String, Integer> asNormalizedMap() {
		Map<String, Integer> normalized = new HashMap<String, Integer>();

		for (Map.Entry<String, AtomicInteger> entry : this.totals.entrySet()) {
			String event = entry.getKey();
			AtomicInteger total = entry.getValue();

			normalized.put(event, total.get());
		}

		return normalized;
	}
}
