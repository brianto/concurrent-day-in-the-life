/**
 * Clock.java
 * Serves as a clock that keeps track of hours and minutes
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class Clock implements Comparable<Clock> {
	private volatile int hour;
	private volatile int minute;
	
	/**
	 * Default constructor. This initializes the clock the the
	 * beginning of the workday, 8:00 AM.
	 */
	public Clock() { this(8, 0); }
	
	/**
	 * Constructor.
	 * @param hour Hour to initialize the clock to.
	 * @param minute Minutes to initialize the clock to.
	 */
	public Clock(int hour, int minute) {
		while (minute >= 60) {
			minute -= 60;
			hour += 1;
		}

		while (minute < 0) {
			minute += 60;
			hour -= 1;
		}
		
		this.hour = hour;
		this.minute = minute;
	}
	
	/**
	 * Copy constructor.
	 */
	public Clock(Clock clock) {
		this(clock.hour, clock.minute);
	}

	/**
	 * Increments the clock time by one minute.
	 * @return This, the clock being incremented.
	 */
	public synchronized Clock increment() {
		if (++this.minute >= 60) {
			this.hour++;
			this.minute = 0;
		}
		return this;
	}
	
	/**
	 * Increments the clock time by the given number of minutes.
	 * @param mins Positive number of minutes to increment the clock by.
	 * @return This, the clock being incremented.
	 */
	public Clock increment(int mins) {
		for (int i = 0; i < mins; i++)
			this.increment();

		return this;
	}
	
	/**
	 * Decrements the clock time by one minute.
	 * @return The clock being decremented.
	 */
	public synchronized Clock decrement() {
		if (--this.minute < 0) {
			this.hour--;
			this.minute = 59;
		}
		return this;
	}
	
	/**
	 * Decrements the clock time by the given number of minutes.
	 * @param mins Positive number of minutes to decrement the clock by.
	 * @return The clock being decremented.
	 */
	public Clock decrement(int mins) {
		for (int i = 0; i < mins; i++)
			this.decrement();

		return this;
	}
	
	/**
	 * Return the current hour.
	 * @return Current hour.
	 */
	public int getHour() {
		return this.hour;
	}
	
	/**
	 * Return the current minute.
	 * @return Current minute.
	 */
	public int getMinute() {
		return this.minute;
	}
	
	/**
	 * Returns a string representation of the Clock object.
	 * @return String representation of Clock object.
	 */
	@Override
	public String toString() {
		return String.format("%02d:%02d %s",
				this.hour > 12 ? this.hour % 13 + 1 : this.hour,
				this.minute, this.hour >= 12 ? "PM" : "AM");
	}
	
	/**
	 * Checks if given Clock equals this Clock.
	 * @param other Clock to compare this Clock to.
	 * @return True if Clocks equal, false otherwise.
	 */
	public synchronized boolean equals(Clock other) {
		return this.hour == other.hour && this.minute == other.minute;
	}
	
	/**
	 * Returns the current time in total minutes elapsed from 8 AM.
	 * @return The total amount of minutes that have passed in the day from 8 AM.
	 */
	public synchronized int value() {
		return (this.hour - 8) * 60 + this.minute;
	}

	/**
	 * Compares two clocks' times.
	 * 
	 * <table>
	 * <thead>
	 * <tr>
	 * <td>Scenario</td>
	 * <td>Interpretation</td>
	 * <td>Sign</td>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>this < other</td>
	 * <td>before</td>
	 * <td>negative</td>
	 * </tr>
	 * <tr>
	 * <td>this = other</td>
	 * <td>same as</td>
	 * <td>zero</td>
	 * </tr>
	 * <tr>
	 * <td>this > other</td>
	 * <td>after</td>
	 * <td>positive</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * 
	 * @param other
	 *            clock with time to stop
	 * @return comparison of this and provided clock's time.
	 */
	@Override
	public int compareTo(Clock other) {
		return this.value() - other.value();
	}
}
