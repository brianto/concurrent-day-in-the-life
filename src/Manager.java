import java.util.HashSet;
import java.util.Set;

/**
 * Manager
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class Manager extends Employee<Manager> {
	private ConferenceRoom office;
	private Set<TeamLead> leads;

	/**
	 * Constructor. Sets up the schedule of the manager for the day.
	 * 
	 * @param clock
	 *            Clock Manager will use to go through the day.
	 */
	public Manager(Clock clock) {
		super(clock, "Manager");

		this.leads = new HashSet<TeamLead>();

		scheduleTask(new Task<Manager>("arriving", 8, 0) {
			@Override
			void action(Manager self) throws Exception {
				log("arrives");

				log("awaiting team leads for standup");

				self.office.getBarrier().await();

				log("started standup");

				waitFor(15);

				log("finished standup");

				self.office.getBarrier().reset();
			}
		});

		scheduleTask(new Task<Manager>("meeting 1", 10, 0) {
			@Override
			void action(Manager self) throws Exception {
				log("joined meeting 1");

				waitFor(60);

				log("left meeting 1");
			}
		});

		scheduleTask(new Task<Manager>("lunch", 12, 0) {
			@Override
			void action(Manager self) throws Exception {
				log("starting lunch");

				waitFor(60);

				log("finished lunch");

			}
		});

		scheduleTask(new Task<Manager>("meeting 2", 14, 0) {
			@Override
			void action(Manager self) throws Exception {
				log("joined meeting 2");

				waitFor(60);

				log("finished meeting 2");
			}
		});

		scheduleTask(new Task<Manager>("leaving", 17, 0) {
			@Override
			void action(Manager self) throws Exception {
				log("leaves");

				leave();
			}
		});

	}

	/**
	 * Register a team lead this manager supervises.
	 * 
	 * @param lead
	 *            TeamLead to add
	 * @return this object
	 */
	public Manager addLead(TeamLead lead) {
		this.leads.add(lead);
		return this;
	}

	/**
	 * Registers the manager's office to block on for the morning standup.
	 * 
	 * @param office
	 *            manager's office
	 * @return this object
	 */
	public Manager useOffice(ConferenceRoom office) {
		this.office = office;
		return this;
	}

	@Override
	void answerQuestion(Employee<?> e) throws InterruptedException {
		log("thinking about %s's qusetion", e);

		waitFor(10);

		log("answered %s's question", e);
	}

	@Override
	public Employee<?> getSuperior() {
		throw new RuntimeException(
				"Manager does not have a superior, nor should ask questions");
	}
}
