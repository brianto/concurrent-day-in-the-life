/**
 * Developer
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public class Developer extends Employee<Developer> {
	private TeamLead lead;
	private ConferenceRoom smallConferenceRoom;
	private ConferenceRoom largeConferenceRoom;

	/**
	 * Constructor. Sets up the overall schedule for the Developer.
	 * 
	 * @param clock
	 *            Clock the Developer will follow to coordinate his/her
	 *            activities throughout the day.
	 * @param team
	 *            The number of the development team the developer belongs to.
	 * @param id
	 *            The ID number of the developer in relation to his/her current
	 *            team.
	 */
	public Developer(Clock clock, int team, int id) {
		super(clock, String.format("Developer %d %d", team, id));

		Clock arrival = this.getRandomTimeBetween(Employee.EARLIEST_START,
				new Clock(8, 30), 1);

		Clock leave = new Clock(arrival).increment(8 * 60 + 30);

		scheduleTask(new Task<Developer>("arriving", arrival) {
			@Override
			void action(Developer self) throws Exception {
				log("arrives");
				
				startRecording("waiting");

				while (!self.smallConferenceRoom.usedBy(self.lead))
					Thread.yield();

				log("joined in standup with %s", self.lead);

				self.smallConferenceRoom.getBarrier().await();
				
				stopRecording("waiting");

				log("started standup");
				
				startRecording("meeting");

				waitFor(15);

				log("left standup");
				
				stopRecording("meeting");
				
				startRecording("working");
			}
		});

		// if (this.toString().equals("Developer 1 1") ||
		// this.toString().equals("Developer 1 2"))
		for (int i = 0, stop = this.getRandom().nextInt(5); i < stop; i++)
			this.scheduleRandomQuestion();

		this.scheduleRandomLunch(arrival.getMinute());

		Clock endOfDayStandupArrival = this.getRandomTimeBetween(
				Employee.CLEANUP_TIME, Employee.EVENING_STANDUP_LATEST, 0);

		scheduleTask(new Task<Developer>("end-of-day standup",
				endOfDayStandupArrival) {
			@Override
			void action(Developer self) throws Exception {
				stopRecording("working");
				
				log("arrived at end-of-day standup");
				
				startRecording("waiting");

				self.largeConferenceRoom.getBarrier().await();
				
				stopRecording("waiting");

				log("started end-of-day standup");
				
				startRecording("meeting");
				
				waitFor(15);
				
				log("finished end-of-day standup");
				
				stopRecording("meeting");
				
				startRecording("working");
			}
		});

		scheduleTask(new Task<Developer>("leaving", leave) {
			@Override
			void action(Developer self) throws Exception {
				log("leaves");
				
				stopRecording("working");

				leave();
			}
		});

	}

	/**
	 * Sets the lead of this developer to the given lead.
	 * 
	 * @param lead
	 *            The lead to set over the Developer.
	 * @return This Developer
	 */
	public Developer setLead(TeamLead lead) {
		this.lead = lead;
		return this;
	}

	@Override
	public void answerQuestion(Employee<?> e) {
		throw new RuntimeException(String.format("No, %s", e));
	}

	@Override
	public Employee<?> getSuperior() {
		return this.lead;
	}

	/**
	 * Sets the small conference room the developer is in to the given room.
	 * 
	 * The small conference room is used for the morning team standups.
	 * 
	 * @param conferenceRoom
	 *            Small ConferenceRoom the developer is in.
	 */
	public Developer useSmallConferenceRoom(ConferenceRoom conferenceRoom) {
		this.smallConferenceRoom = conferenceRoom;
		return this;
	}

	/**
	 * Sets the large conference room the developer is in to the given room.
	 * 
	 * The large conference room is used for the larger end-of-day standup with
	 * all developers and leads.
	 * 
	 * @param conferenceRoom
	 *            Large ConferenceRoom the developer is in.
	 */
	public Developer useLargeConferenceRoom(ConferenceRoom conferenceRoom) {
		this.largeConferenceRoom = conferenceRoom;
		return this;
	}
}
