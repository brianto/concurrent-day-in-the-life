/**
 * Task
 * 
 * @author Gabriel Marcano
 * @author Alexander Kelley
 * @author Brian To
 */
public abstract class Task<Role> implements Comparable<Task<Role>> {
	private final Clock time;
	private final String description;

	/**
	 * Constructor.
	 * 
	 * @param description
	 *            Description of the current task.
	 * @param hour
	 *            Hour (24 hour format) the task takes place in
	 * @param minute
	 *            Minute the task takes place in
	 */
	public Task(String description, int hour, int minute) {
		this(description, new Clock(hour, minute));
	}

	/**
	 * Constructor.
	 * 
	 * The <code>time</code> parameter is copied, not referenced.
	 * 
	 * @param description
	 *            Description of the current task.
	 * @param time
	 *            Time the task takes place in.
	 */
	public Task(String description, Clock time) {
		this.time = new Clock(time);
		this.description = description;
	}

	/**
	 * Checks if the current task should run.
	 * 
	 * @return True if the task should run, false otherwise.
	 */
	public boolean shouldRun(Clock target) {
		return this.time.compareTo(target) <= 0;
	}

	/**
	 * Returns the time the task should run
	 * 
	 * @return A copy of the clock time the task should run.
	 */
	public Clock getClock() {
		return new Clock(this.time);
	}

	@Override
	public int compareTo(Task<Role> other) {
		return this.time.value() - other.getClock().value();
	}

	/**
	 * Executes the client-implemented {@link Task#action(Object)}, but casts
	 * the employee to the proper subtype.
	 */
	@SuppressWarnings("unchecked")
	public void doActionWrapper(Employee<Role> e) throws Exception {
		action((Role) e);
	}

	/**
	 * Client (a subclass of {@link Employee}) implementation of a task, such as
	 * a meeting or lunch.
	 * 
	 * The self parameter is a convenience for the programmer to access the
	 * enclosing instance. <code>self</code> is equivalent to:
	 * 
	 * <code>
	 * 	Role self = Role.this;
	 * </code>
	 * 
	 * @param self
	 *            the <code>this</code> instance of the <emph>casted</emph>
	 *            employee instance for convenience use.
	 */
	abstract void action(Role self) throws Exception;

	/**
	 * Gets the description of the task.
	 * 
	 * @return Description of the task.
	 */
	public String getDescription() {
		return this.description;
	}
}
