Project Concurrent Day in the Life
Brian To, Alexander Kelley, Gabriel Marcano

In order to run the program:
java Main [random-seed]

random-seed				a seed passed to the random generator to produce a
						predictable run; multiple runs with the same seed
						should yield similar output aside from same-time ordering

In order to compile the program:
Compile all of the source files in the same folder. In Bash, this could be done with:
javac *.java
